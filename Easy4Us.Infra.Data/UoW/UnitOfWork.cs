﻿using Easy4Us.Domain.Interfaces.Repositories;
using Easy4Us.Infra.Data.Context;
using System.Threading.Tasks;

namespace Easy4Us.Infra.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Easy4UsContext _context;

        public UnitOfWork(Easy4UsContext context)
        {
            _context = context;
        }

        public bool Commit()
        {
            return _context.SaveChanges() > 0;
        }

        public async Task<bool> CommitAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
