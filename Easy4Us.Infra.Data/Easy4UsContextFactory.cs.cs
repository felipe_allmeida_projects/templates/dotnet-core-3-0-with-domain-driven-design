﻿using Easy4Us.Infra.Data.Context;
using Easy4Us.Infra.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy4Us.Infra.Data
{
    public class Easy4UsContextFactory : IDesignTimeDbContextFactory<Easy4UsContext>
    {
        public Easy4UsContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<Easy4UsContext>();
            var connStr = ConfigurationHelper.GetCurrentSettings("ConnectionStrings:DefaultConnection");
            optionsBuilder.UseSqlServer(connStr);
            return new Easy4UsContext(optionsBuilder.Options);
        }
    }
}
