﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Easy4Us.Infra.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Campaing",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campaing", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ISR = table.Column<string>(nullable: true),
                    CustomerNumber = table.Column<string>(nullable: true),
                    CGC = table.Column<string>(nullable: true),
                    Company = table.Column<string>(nullable: true),
                    Campaing = table.Column<string>(nullable: true),
                    GAMA = table.Column<bool>(nullable: false),
                    SFDC = table.Column<bool>(nullable: false),
                    NewEasy = table.Column<bool>(nullable: false),
                    USP = table.Column<bool>(nullable: false),
                    QuoteTime = table.Column<int>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Campaing");

            migrationBuilder.DropTable(
                name: "Customer");
        }
    }
}
