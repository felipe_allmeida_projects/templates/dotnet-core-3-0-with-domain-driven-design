﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Easy4Us.Infra.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Campaign",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campaign", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ISR = table.Column<string>(nullable: true),
                    CustomerNumber = table.Column<string>(nullable: true),
                    FederalTaxID = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    Campaign = table.Column<string>(nullable: true),
                    GAMA = table.Column<int>(nullable: false),
                    SFDC = table.Column<int>(nullable: false),
                    NewEasy = table.Column<int>(nullable: false),
                    USP = table.Column<int>(nullable: false),
                    QuoteTime = table.Column<int>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Campaign");

            migrationBuilder.DropTable(
                name: "Customer");
        }
    }
}
