﻿using Easy4Us.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Easy4Us.Infra.Data.Context
{
    public class Easy4UsContext : DbContext
    {
        public DbSet<Campaign> Campaigns { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public Easy4UsContext(DbContextOptions<Easy4UsContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Campaign>().ToTable("Campaign");
            modelBuilder.Entity<Customer>().ToTable("Customer");
        }
    }
}
 