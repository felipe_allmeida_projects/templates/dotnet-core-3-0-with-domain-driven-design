﻿using Easy4Us.Domain.Core.Models;
using Easy4Us.Domain.Interfaces;
using Easy4Us.Domain.Interfaces.Repositories.Base;
using Easy4Us.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy4Us.Infra.Data.Repository.Base
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly Easy4UsContext Db;
        protected readonly DbSet<TEntity> DbSet;

        public BaseRepository(Easy4UsContext context)
        {
            Db = context;
            DbSet = Db.Set<TEntity>();
        }

        public bool Exists(int id)
        {
            return DbSet.Any(x => x.ID == id);
        }

        public async Task<bool> ExistsAsync(int id)
        {
            return await DbSet.AnyAsync(x => x.ID == id);
        }

        public virtual TEntity Add(TEntity entity)
        {
            DbSet.Add(entity);
            return entity;
        }

        public IEnumerable<TEntity> AddRange(IEnumerable<TEntity> entities)
        {
            DbSet.AddRange(entities);
            return entities;
        }

        public virtual async Task<TEntity> AddAsync(TEntity entity)
        {
            await DbSet.AddAsync(entity);
            return entity;
        }

        public virtual async Task<IEnumerable<TEntity>> AddRangeAsync(IEnumerable<TEntity> entities)
        {
            await DbSet.AddRangeAsync(entities);
            return entities;
        }

        public virtual TEntity GetById(int id)
        {
            return DbSet.FirstOrDefault(x => x.ID == id);
        }

        public virtual async Task<TEntity> GetByIdAsync(int id)
        {
            return await DbSet.FirstOrDefaultAsync(x => x.ID == id);
        }

        public virtual async Task<IEnumerable<TEntity>> GetAllQuery()
        {
            return await DbSet.ToListAsync();
        }

        public virtual void Update(TEntity obj)
        {
            DbSet.Attach(obj);
            Db.Entry(obj).State = EntityState.Modified;
        }

        public virtual void Remove(int id)
        {
            DbSet.Remove(DbSet.Find(id));
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            DbSet.RemoveRange(entities);
        }

        public async Task<int> SaveChanges()
        {
            return await Db.SaveChangesAsync();
        }

        public void Dispose()
        {
            Db.Dispose();
            GC.SuppressFinalize(this);
        }   
    }
}
