﻿using Easy4Us.Domain.Interfaces.Repositories;
using Easy4Us.Domain.Models;
using Easy4Us.Infra.Data.Context;
using Easy4Us.Infra.Data.Repository.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy4Us.Infra.Data.Repository
{
    public class CampaignRepository : BaseRepository<Campaign>, ICampaignRepository
    {
        public CampaignRepository(Easy4UsContext context) : base(context)
        {

        }
    }
}
