﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Easy4Us.Domain.Interfaces.Repositories;
using Easy4Us.Domain.Models;
using Easy4Us.Infra.Data.Context;
using Easy4Us.Infra.Data.Repository.Base;
using Microsoft.EntityFrameworkCore;

namespace Easy4Us.Infra.Data.Repository
{
    public class CustomerRepository : BaseRepository<Customer>, ICustomerRepository
    {
        
        public CustomerRepository(Easy4UsContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Customer>> GetCustomers(string campaign, string isr)
        {
            return await (from customer in Db.Customers
                         where customer.Campaign == campaign && customer.ISR == isr
                         select customer).ToListAsync();
        }
    }
}
