﻿using Easy4Us.Application.Interfaces;
using Easy4Us.Application.Services;
using Easy4Us.Domain.Core.Interfaces;
using Easy4Us.Domain.Core.Notifications;
using Easy4Us.Domain.Interfaces.Repositories;
using Easy4Us.Domain.Interfaces.Services;
using Easy4Us.Domain.Services;
using Easy4Us.Infra.CrossCutting.Common.Helpers;
using Easy4Us.Infra.CrossCutting.Common.Interfaces;
using Easy4Us.Infra.CrossCutting.Log.Interfaces;
using Easy4Us.Infra.CrossCutting.Log.Services;
using Easy4Us.Infra.CrossCutting.SharePoint.Interfaces;
using Easy4Us.Infra.CrossCutting.SharePoint.Services;
using Easy4Us.Infra.Data.Repository;
using Easy4Us.Infra.Data.UoW;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Easy4Us.Infra.CrossCutting.IoC
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            RegisterPresentation(services);
            RegisterApplication(services);
            RegisterDomainCore(services);
            RegisterInfraData(services);
            RegisterInfraCrossCutting(services);
        }

        private static void RegisterPresentation(IServiceCollection services)
        {   
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        private static void RegisterApplication(IServiceCollection services)
        {
            services.AddScoped<ICustomerAppService, CustomerAppService>();
            services.AddScoped<ICampaignAppService, CampaignAppService>();
        }

        private static void RegisterDomainCore(IServiceCollection services)
        {
            services.AddScoped<IHandler<DomainNotification>, DomainNotificationHandler>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<ICampaignService, CampaignService>();
            services.AddScoped<IValidateService, ValidateService>();
        }

        private static void RegisterInfraData(IServiceCollection services)
        {
            services.AddScoped<ILogger, LoggerService>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<ICampaignRepository, CampaignRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }

        private static void RegisterInfraCrossCutting(IServiceCollection services)
        {           
            services.AddScoped<IUserAccessor, UserAccessor>();
            services.AddScoped<ISharePointService, SharePointService>();
        }
    }
}
