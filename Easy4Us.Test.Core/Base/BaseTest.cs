﻿using AutoMapper;
using Easy4Us.Application.AutoMapper;
using Easy4Us.Domain.Core.Interfaces;
using Easy4Us.Domain.Core.Notifications;
using Moq;

namespace Easy4Us.Test.Core.Base
{
    public abstract class BaseTest
    {
        protected Mock<IHandler<DomainNotification>> _moqNotification { get; private set; }   
        protected IMapper _mapper { get; private set; }

        public BaseTest()
        {
            _moqNotification = new Mock<IHandler<DomainNotification>>();

            var mapperFactory = new MapperConfiguration(cfg =>
            {
                cfg.AllowNullDestinationValues = true;
                cfg.AllowNullCollections = true;

                cfg.DisableConstructorMapping();

                cfg.ForAllMaps((mapType, mapperExpression) =>
                {
                    mapperExpression.MaxDepth(1);
                });

                cfg.AddProfile(new DomainToDTOMappingProfile());
                cfg.AddProfile(new DTOToDomainMappingProfile());
            });

            _mapper = mapperFactory.CreateMapper();
        }
    }
}
