﻿using Easy4Us.Domain.Interfaces.Repositories;
using Easy4Us.Infra.Data.UoW;
using Easy4Us.Test.Core.Base;
using Easy4Us.Test.Core.Data;

namespace Easy4Us.Test.Core
{
    public class BaseIntegrationTest : BaseTest
    {
        protected readonly DbInMemory _dbInMemory;

        protected readonly IUnitOfWork _unitOfWork;        

        public BaseIntegrationTest(DbInMemory dbInMemory) : base()
        {
            _dbInMemory = dbInMemory;

            _unitOfWork = new UnitOfWork(_dbInMemory.Context);
        }
    }
}
