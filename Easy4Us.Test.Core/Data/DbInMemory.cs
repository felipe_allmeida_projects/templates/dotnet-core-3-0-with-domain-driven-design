﻿using Easy4Us.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy4Us.Test.Core.Data
{
    public class DbInMemory : IDisposable
    {
        public Easy4UsContext Context { get; private set; }

        public DbInMemory()
        {
            var options = new DbContextOptionsBuilder<Easy4UsContext>()
                   .UseInMemoryDatabase(databaseName: "DefaultConnection")
                   .Options;


            Context = new Easy4UsContext(options);
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
