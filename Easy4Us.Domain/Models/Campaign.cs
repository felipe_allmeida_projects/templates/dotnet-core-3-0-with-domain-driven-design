﻿using Easy4Us.Domain.Core.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace Easy4Us.Domain.Models
{
    public class Campaign : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
