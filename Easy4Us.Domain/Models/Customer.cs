﻿using Easy4Us.Domain.Core.Models;
using Easy4Us.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy4Us.Domain.Models
{
    public class Customer : BaseEntity
    {
        public string ISR { get; set; }
        public string CustomerNumber { get; set; }
        public string FederalTaxID { get; set; }
        public string CompanyName { get; set; }
        public string Campaign { get; set; }
        public ValidationStatus GAMA { get; set; }
        public ValidationStatus SFDC { get; set; }
        public ValidationStatus NewEasy { get; set; }
        public ValidationStatus USP { get; set; }
        public int QuoteTime { get; set; }
        public DateTime LastUpdated { get; set; }
    }
}
