﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy4Us.Domain.Enums
{
    public enum ValidationStatus
    {
        NOT_VALIDATED,
        INVALID,
        VALID
    }
}
