﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Easy4Us.Domain.Core.Interfaces;
using Easy4Us.Domain.Core.Notifications;
using Easy4Us.Domain.Interfaces.Repositories;
using Easy4Us.Domain.Interfaces.Services;
using Easy4Us.Domain.Models;
using Easy4Us.Domain.Services.Base;
using Easy4Us.Infra.CrossCutting.SharePoint.Interfaces;
using Easy4Us.Infra.CrossCutting.SharePoint.Models;

namespace Easy4Us.Domain.Services
{
    public class CustomerService : BaseService<Customer>, ICustomerService
    {
        private readonly ISharePointService _sharePointService;
        private readonly IValidateService _validateService;
        private readonly ICustomerRepository _repository;
        private readonly IMapper _mapper;
        public CustomerService(ISharePointService sharePointService, IValidateService validateService,
            IMapper mapper, ICustomerRepository repository, IHandler<DomainNotification> notifications)
            : base(repository, notifications)
        {
            _sharePointService = sharePointService;
            _validateService = validateService;
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<IEnumerable<Customer>> GetCustomers(string campaign, string isr)
        {
            return await _repository.GetCustomers(campaign, isr);
        }

        public async Task<IEnumerable<Customer>> Refresh(string campaign, string isr)
        {
            if (!string.IsNullOrEmpty(isr))
            {
                var dbCustomers = await GetCustomers(campaign, isr);
                _repository.RemoveRange(dbCustomers);
            }

            // Get Stronger Customers from Stronger SharePoint
            var strongerCustomers = await _sharePointService.GetStrongerCustomers(isr);
            
            // Select only target campaign
            strongerCustomers = strongerCustomers.Where(x => x.Campaign == campaign);

            if (strongerCustomers.Count() > 0)
            {
                // Convert StrongerCustomer to Customer
                var customers = _mapper.Map<IEnumerable<StrongerCustomer>, List<Customer>>(strongerCustomers);

                // Validate customers
                await _validateService.ValidateMany(customers);

                // Save
                return _repository.AddRange(customers);   
            }

            return new List<Customer>();
        }
    }
}
