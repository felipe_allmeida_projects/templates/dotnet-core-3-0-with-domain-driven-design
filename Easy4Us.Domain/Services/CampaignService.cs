﻿using Easy4Us.Domain.Core.Interfaces;
using Easy4Us.Domain.Core.Notifications;
using Easy4Us.Domain.Interfaces.Repositories;
using Easy4Us.Domain.Interfaces.Services;
using Easy4Us.Domain.Models;
using Easy4Us.Domain.Services.Base;

namespace Easy4Us.Domain.Services
{
    public class CampaignService : BaseService<Campaign>, ICampaignService
    {
        private readonly ICampaignRepository _repository;

        public CampaignService(ICampaignRepository repository, IHandler<DomainNotification> notifications)
            : base(repository, notifications)
        {
            _repository = repository;
        }
    }
}
