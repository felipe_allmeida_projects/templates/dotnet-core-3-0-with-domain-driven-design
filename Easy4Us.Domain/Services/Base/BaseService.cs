﻿using Easy4Us.Domain.Core.Interfaces;
using Easy4Us.Domain.Core.Models;
using Easy4Us.Domain.Core.Notifications;
using Easy4Us.Domain.Interfaces.Repositories.Base;
using Easy4Us.Domain.Interfaces.Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy4Us.Domain.Services.Base
{
    public class BaseService<TEntity> : IBaseService<TEntity> where TEntity : BaseEntity
    {
        protected IHandler<DomainNotification> Notifications { get; }
        protected IBaseRepository<TEntity> BaseRepository { get; }

        public BaseService(IBaseRepository<TEntity> baseRepository, IHandler<DomainNotification> notifications)
        {
            BaseRepository = baseRepository;
            Notifications = notifications;
        }

        public virtual bool Exists(int id)
        {
            return BaseRepository.Exists(id);
        }

        public virtual async Task<bool> ExistsAsync(int id)
        {
            return await BaseRepository.ExistsAsync(id);
        }

        public TEntity GetById(int id)
        {
            if (id == default) return default;

            return BaseRepository.GetById(id);
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            if (id == default) return default;

            return await BaseRepository.GetByIdAsync(id);
        }
        public async Task<IEnumerable<TEntity>> GetAll()
        {
            return await BaseRepository.GetAllQuery();
        }

        public TEntity Add(TEntity entity)
        {
            return BaseRepository.Add(entity);
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            return await BaseRepository.AddAsync(entity);
        }

        public void Update(TEntity entity)
        {
            BaseRepository.Update(entity);
        }

        public void Remove(int id)
        {
            BaseRepository.Remove(id);
        }
    }
}
