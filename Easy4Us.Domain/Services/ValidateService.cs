﻿using Easy4Us.Domain.Enums;
using Easy4Us.Domain.Interfaces.Services;
using Easy4Us.Domain.Models;
using Easy4Us.Infra.CrossCutting.SharePoint.Interfaces;
using Easy4Us.Infra.CrossCutting.SharePoint.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Easy4Us.Domain.Services
{
    public class ValidateService : IValidateService
    {
        private readonly ISharePointService _sharePointService;
        public ValidateService(ISharePointService sharePointService)
        {
            _sharePointService = sharePointService;
        }

        public async Task ValidateMany(IEnumerable<Customer> customers)
        {
            Task tasksNewEasy = ValidateNewEasy(customers);

            await Task.WhenAll(tasksNewEasy);
        }

        private async Task ValidateNewEasy(IEnumerable<Customer> customers)
        {
            IEnumerable<NewEasyCustomer> newEasyCustomers = await _sharePointService.GetNewEasyCustomers();

            foreach(var customer in customers)
            {
                var result = newEasyCustomers.FirstOrDefault(x =>
                {
                    return x.CompanyName.ToLower() == customer.CompanyName.ToLower() || x.FederalTaxID.ToLower() == customer.FederalTaxID.ToLower();
                });

                customer.NewEasy = result == null ? ValidationStatus.VALID : ValidationStatus.INVALID;
            }
        }

        private async Task ValidateGama(Customer customer)
        {
        }

        private async Task ValidateSFDC(Customer customer)
        {
        }

        private async Task<Customer> ValidateUSP(Customer customer)
        {
            return customer;
        }
    }
}
