﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Easy4Us.Domain.Interfaces.Repositories.Base
{
    public interface IBaseRepository<TEntity> : IDisposable where TEntity : class
    {
        Task<IEnumerable<TEntity>> GetAllQuery();
        bool Exists(int id);
        Task<bool> ExistsAsync(int id);
        TEntity Add(TEntity entity);
        IEnumerable<TEntity> AddRange(IEnumerable<TEntity> entities);
        Task<TEntity> AddAsync(TEntity entity);
        Task<IEnumerable<TEntity>> AddRangeAsync(IEnumerable<TEntity> entities);
        TEntity GetById(int id);
        Task<TEntity> GetByIdAsync(int id);
        void Update(TEntity entity);
        void Remove(int id);
        void RemoveRange(IEnumerable<TEntity> entities);
        Task<int> SaveChanges();
    }
}
