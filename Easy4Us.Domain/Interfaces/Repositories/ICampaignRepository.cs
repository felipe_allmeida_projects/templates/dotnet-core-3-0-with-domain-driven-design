﻿using Easy4Us.Domain.Interfaces.Repositories.Base;
using Easy4Us.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy4Us.Domain.Interfaces.Repositories
{
    public interface ICampaignRepository : IBaseRepository<Campaign>
    {
    }
}
