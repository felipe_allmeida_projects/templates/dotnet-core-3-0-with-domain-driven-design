﻿using System;
using System.Threading.Tasks;

namespace Easy4Us.Domain.Interfaces.Repositories
{
    public interface IUnitOfWork : IDisposable   
    {
        bool Commit();
        Task<bool> CommitAsync();
    }
}
