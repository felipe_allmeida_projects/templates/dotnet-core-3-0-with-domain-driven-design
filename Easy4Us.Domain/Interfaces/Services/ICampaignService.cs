﻿using Easy4Us.Domain.Interfaces.Services.Base;
using Easy4Us.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy4Us.Domain.Interfaces.Services
{
    public interface ICampaignService : IBaseService<Campaign>
    {

    }
}
