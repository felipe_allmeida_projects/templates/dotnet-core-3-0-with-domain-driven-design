﻿using Easy4Us.Domain.Interfaces.Services.Base;
using Easy4Us.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Easy4Us.Domain.Interfaces.Services
{
    public interface ICustomerService : IBaseService<Customer>
    {
        Task<IEnumerable<Customer>> GetCustomers(string campaign, string isr);

        Task<IEnumerable<Customer>> Refresh(string campaign, string isr);
    }
}
