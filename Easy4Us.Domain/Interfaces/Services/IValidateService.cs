﻿using Easy4Us.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Easy4Us.Domain.Interfaces.Services
{
    public interface IValidateService
    {
        Task ValidateMany(IEnumerable<Customer> customer);
    }
}
