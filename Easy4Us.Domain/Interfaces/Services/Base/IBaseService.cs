﻿using Easy4Us.Domain.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Easy4Us.Domain.Interfaces.Services.Base
{
    public interface IBaseService<TEntity> where TEntity : BaseEntity
    {
        bool Exists(int id);
        Task<bool> ExistsAsync(int id);
        TEntity GetById(int id);
        Task<TEntity> GetByIdAsync(int id);
        Task<IEnumerable<TEntity>> GetAll();
        TEntity Add(TEntity entity);
        Task<TEntity> AddAsync(TEntity entity);
        void Update(TEntity entity);
        void Remove(int id);
    }
}
