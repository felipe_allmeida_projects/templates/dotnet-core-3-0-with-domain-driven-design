﻿using Easy4Us.Domain.Interfaces.Services.Base;
using Easy4Us.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Easy4Us.Domain.Interfaces
{
    public interface ICustomerService : IBaseService<Customer>
    {
        Task<IEnumerable<Customer>> GetCustomersForISR(string isr);

        Task Refresh(string isr, IEnumerable<Customer> customers);
    }
}
