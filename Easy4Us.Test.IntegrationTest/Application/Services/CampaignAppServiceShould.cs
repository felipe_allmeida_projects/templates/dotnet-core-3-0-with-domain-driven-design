﻿using Easy4Us.Application.DTO;
using Easy4Us.Application.Services;
using Easy4Us.Domain.Models;
using Easy4Us.Domain.Services;
using Easy4Us.Infra.Data.Repository;
using Easy4Us.Test.Core;
using Easy4Us.Test.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Easy4Us.Test.IntegrationTest.Application.Services
{
    public class CampaignAppServiceShould : BaseIntegrationTest, IClassFixture<DbInMemory>
    {
        private readonly CampaignAppService _campaignAppService;
        public CampaignAppServiceShould(DbInMemory dbInMemory) : base(dbInMemory)
        {
            var campaignRepository = new CampaignRepository(_dbInMemory.Context);
            var campaignService = new CampaignService(campaignRepository, _moqNotification.Object);

            _campaignAppService = new CampaignAppService(_moqNotification.Object, _unitOfWork, campaignService, _mapper);

            PopulateDbInMemory();
        }

        [Fact]
        public async Task ShouldReturnAllCampaigns()
        {
            var campaigns = await _campaignAppService.GetCampaigns();

            Assert.NotNull(campaigns);
            Assert.Equal(2, campaigns.Count());
        }

        [Fact]
        public async Task ShouldAddCampaign()
        {
            var campaignDTO = await _campaignAppService.CreateCampaign(new CampaignDTO()
            {
                Name = "Test",
                Description = ""
            });

            Assert.True(campaignDTO.ID != 0);
        }

        [Fact]
        public async Task ShouldDeleteCampaign()
        {
            await _campaignAppService.DeleteCampaign(2);

            var result = await _campaignAppService.GetCampaign(2);

            Assert.Null(result);
        }

        private void PopulateDbInMemory()
        {
            List<Campaign> campaigns = new List<Campaign>()
            {
                new Campaign() { Name = "OPEN QUOTES", Description = "Ticket 'dell20years' discount 20%" },
                new Campaign() { Name = "CANCELAMENTOS", Description = "" }
            };

            _dbInMemory.Context.Campaigns.AddRange(campaigns);

            _dbInMemory.Context.SaveChanges();
        }
    }
}
