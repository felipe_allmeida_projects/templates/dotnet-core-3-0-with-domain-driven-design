﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy4Us.Infra.CrossCutting.Log.Interfaces
{
    public interface ILogger
    {
        void LogInfo(string infoMessage);
        void LogWarning(string warningMessage);
        void LogError(Exception ex, string message);
    }
}
