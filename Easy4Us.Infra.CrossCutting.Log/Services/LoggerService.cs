﻿using Easy4Us.Infra.CrossCutting.Log.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy4Us.Infra.CrossCutting.Log.Services
{
    public class LoggerService : ILogger
    {
        public void LogError(Exception ex, string message)
        {
            Console.Error.WriteLine(string.Format("ERROR MESSAGE: {0}, ERROR: {1}", message, ex));
        }

        public void LogInfo(string infoMessage)
        {
            Console.Out.WriteLine(string.Format("[INFO]: {0}", infoMessage));
        }

        public void LogWarning(string warningMessage)
        {
            Console.Out.WriteLine(string.Format("[WARNING]: {0}", warningMessage));
        }
    }
}
