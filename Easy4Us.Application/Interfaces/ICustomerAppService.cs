﻿using Easy4Us.Application.DTO;
using Easy4Us.Application.Interfaces.Base;
using Easy4Us.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Easy4Us.Application.Interfaces
{
    public interface ICustomerAppService : IBaseAppService<CustomerDTO>
    {
        Task<IEnumerable<CustomerDTO>> GetCustomers(string campaign, string isr);
        Task<IEnumerable<CustomerDTO>> Refresh(string campaign, string isr);
    }
}
