﻿using Easy4Us.Application.DTO;
using Easy4Us.Application.Interfaces.Base;
using Easy4Us.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Easy4Us.Application.Interfaces
{
    public interface ICampaignAppService : IBaseAppService<CampaignDTO>
    {
        Task<IEnumerable<CampaignDTO>> GetCampaigns();
        Task<CampaignDTO> GetCampaign(int id);
        Task<CampaignDTO> CreateCampaign(CampaignDTO campaignDTO);
        Task<CampaignDTO> UpdateCampaign(CampaignDTO campaignDTO);
        Task DeleteCampaign(int id);
    }
}
