﻿using Easy4Us.Application.DTO.Base;
using System;
using System.Threading.Tasks;

namespace Easy4Us.Application.Interfaces.Base
{
    public interface IBaseAppService<TDTO> : IDisposable
        where TDTO : BaseDTO, new()
    {
    }
}
