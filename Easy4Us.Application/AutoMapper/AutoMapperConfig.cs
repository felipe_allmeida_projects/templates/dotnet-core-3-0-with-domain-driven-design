﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy4Us.Application.AutoMapper
{
    public static class AutoMapperConfig
    {
        public static MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AllowNullDestinationValues = true;
                cfg.AllowNullCollections = true;
                
                cfg.DisableConstructorMapping();

                cfg.ForAllMaps((mapType, mapperExpression) => mapperExpression.MaxDepth(1));

                cfg.AddProfile(new DomainToDTOMappingProfile());
                cfg.AddProfile(new DTOToDomainMappingProfile());
            });
        }
    }
}
