﻿using AutoMapper;
using Easy4Us.Application.DTO;
using Easy4Us.Domain.Models;
using Easy4Us.Infra.CrossCutting.SharePoint.Models;

namespace Easy4Us.Application.AutoMapper
{
    public class DomainToDTOMappingProfile : Profile
    {
        public DomainToDTOMappingProfile()
        {
            CreateMap<Customer, StrongerCustomer>().ReverseMap();
            CreateMap<Customer, CustomerDTO>().ReverseMap();
            CreateMap<Campaign, CampaignDTO>().ReverseMap();
        }
    }
}
