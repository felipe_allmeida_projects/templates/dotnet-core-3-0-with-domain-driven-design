﻿using Easy4Us.Application.DTO.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Easy4Us.Application.DTO
{
    public class CustomerStrongerDTO : BaseDTO
    {
        public string ISR { get; set; }
        public string CustomerNumber { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string FederalTaxID { get; set; }
        public string Companiy { get; set; }
        public string Campaign { get; set; }
    }
}
