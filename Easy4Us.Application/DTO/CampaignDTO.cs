﻿using Easy4Us.Application.DTO.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Easy4Us.Application.DTO
{
    public class CampaignDTO : BaseDTO
    {
        public int ID { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name cannot be empty.")]
        public string Name { get; set; }
        [StringLength(280, ErrorMessage = "Description is too long.")]
        public string Description { get; set; }
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
    }
}
