﻿using AutoMapper;
using Easy4Us.Application.DTO;
using Easy4Us.Application.Interfaces;
using Easy4Us.Application.Services.Base;
using Easy4Us.Domain.Core.Interfaces;
using Easy4Us.Domain.Core.Notifications;
using Easy4Us.Domain.Interfaces.Repositories;
using Easy4Us.Domain.Interfaces.Services;
using Easy4Us.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Easy4Us.Application.Services
{
    public class CampaignAppService : BaseAppDTODomainService<CampaignDTO, Campaign>, ICampaignAppService
    {
        private readonly ICampaignService _service;
        public CampaignAppService(IHandler<DomainNotification> notifications, IUnitOfWork unitOfWork,
            ICampaignService service, IMapper mapper)
            : base(notifications, unitOfWork, service, mapper)
        {
            _service = service;
        }

        public async Task<IEnumerable<CampaignDTO>> GetCampaigns()
        {
            var campaigns = await _service.GetAll();

            return Mapper.Map<IEnumerable<Campaign>, IEnumerable<CampaignDTO>>(campaigns);
        }

        public async Task<CampaignDTO> GetCampaign(int id)
        {
            var campaign = await _service.GetByIdAsync(id);

            return Mapper.Map<CampaignDTO>(campaign);
        }

        public async Task<CampaignDTO> CreateCampaign(CampaignDTO campaignDTO)
        {
            var campaign = Mapper.Map<Campaign>(campaignDTO);

            await _service.AddAsync(campaign);

            await CommitAsync();

            return Mapper.Map<CampaignDTO>(campaign);
        }

        public async Task<CampaignDTO> UpdateCampaign(CampaignDTO campaignDTO)
        {
            var campaign = Mapper.Map<Campaign>(campaignDTO);

            _service.Update(campaign);

            await CommitAsync();

            return Mapper.Map<CampaignDTO>(campaign);
        }

        public async Task DeleteCampaign(int id)
        {
            _service.Remove(id);
            
            await CommitAsync();
        }
    }
}
