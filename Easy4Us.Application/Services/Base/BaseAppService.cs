﻿using Easy4Us.Domain.Core.Interfaces;
using Easy4Us.Domain.Core.Notifications;
using Easy4Us.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Easy4Us.Application.Services.Base
{
    public class BaseAppService
    {
        private readonly IUnitOfWork _unitOfWork;
        protected IHandler<DomainNotification> Notifications { get; }
        public BaseAppService(IUnitOfWork unitOfWork, IHandler<DomainNotification> notifications)
        {
            _unitOfWork = unitOfWork;
            Notifications = notifications;
        }

        protected bool Commit()
        {
            if (Notifications.HasNotifications())
            {
                return false;
            }

            return _unitOfWork.Commit();
        }

        protected async Task<bool> CommitAsync()
        {
            if (Notifications.HasNotifications())
            {
                return false;
            }

            return await _unitOfWork.CommitAsync();
        }
    }
}
