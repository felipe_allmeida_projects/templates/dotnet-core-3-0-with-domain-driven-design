﻿using AutoMapper;
using Easy4Us.Application.DTO.Base;
using Easy4Us.Application.Interfaces.Base;
using Easy4Us.Domain.Core.Interfaces;
using Easy4Us.Domain.Core.Models;
using Easy4Us.Domain.Core.Notifications;
using Easy4Us.Domain.Interfaces.Repositories;
using Easy4Us.Domain.Interfaces.Services.Base;
using System;
using System.Threading.Tasks;

namespace Easy4Us.Application.Services.Base
{
    public class BaseAppDTODomainService<TDTO, TDomainModel> : BaseAppService, IBaseAppService<TDTO>
        where TDTO : BaseDTO, new()
        where TDomainModel : BaseEntity, new()
    {
        protected IBaseService<TDomainModel> BaseDomainService { get; }
        protected IMapper Mapper { get; }

        public BaseAppDTODomainService(IHandler<DomainNotification> notifications, IUnitOfWork unitOfWork, IBaseService<TDomainModel> baseService, IMapper mapper)
            : base(unitOfWork, notifications)
        {
            BaseDomainService = baseService;
            Mapper = mapper;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
