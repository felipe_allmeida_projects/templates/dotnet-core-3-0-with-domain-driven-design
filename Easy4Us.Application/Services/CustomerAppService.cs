﻿using AutoMapper;
using Easy4Us.Application.DTO;
using Easy4Us.Application.Interfaces;
using Easy4Us.Application.Services.Base;
using Easy4Us.Domain.Core.Interfaces;
using Easy4Us.Domain.Core.Notifications;
using Easy4Us.Domain.Interfaces.Repositories;
using Easy4Us.Domain.Interfaces.Services;
using Easy4Us.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Easy4Us.Application.Services
{
    public class CustomerAppService : BaseAppDTODomainService<CustomerDTO, Customer>, ICustomerAppService
    {
        private readonly ICustomerService _customerService;
        public CustomerAppService(IHandler<DomainNotification> notifications, IUnitOfWork unitOfWork, 
            ICustomerService customerService, IMapper mapper)
            : base(notifications, unitOfWork, customerService, mapper)
        {
            _customerService = customerService;
        }

        public async Task<IEnumerable<CustomerDTO>> GetCustomers(string campaign, string isr)
        {
            var customers = await _customerService.GetCustomers(campaign, isr);

            var customersDTO = Mapper.Map<IEnumerable<Customer>, IEnumerable<CustomerDTO>>(customers);

            return customersDTO;
        }

        public async Task<IEnumerable<CustomerDTO>> Refresh(string campaign, string isr)
        {
            var customers = await _customerService.Refresh(campaign, isr);

            var customersDTO = Mapper.Map<IEnumerable<Customer>, IEnumerable<CustomerDTO>>(customers);

            await CommitAsync();

            return customersDTO;
        }
    }
}
