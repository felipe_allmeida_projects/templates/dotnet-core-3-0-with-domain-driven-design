﻿using AutoMapper;
using Easy4Us.Application.DTO;
using Easy4Us.Domain.Models;
using Easy4Us.Test.Core;
using System;
using Xunit;

namespace Easy4Us.Test.UnitTest.Application.AutoMapper
{
    public class ObjectMappingTests : BaseUnitTest
    {
        [Fact]
        public void ConvertCustomerToCustomerDTO()
        {
            Customer customer = new Customer()
            {
                ID = 1,
                ISR = "Viera, Aline",
                LastUpdated = DateTime.MinValue,
                Campaign = "OPEN QUOTES",
                CustomerNumber = "113576911",
                FederalTaxID = "CPF 80849415187",
                CompanyName = "WAGNER AUGUSTO DO AMARAL",
                QuoteTime = 0,
                GAMA = Domain.Enums.ValidationStatus.VALID,
                NewEasy = Domain.Enums.ValidationStatus.VALID,
                SFDC = Domain.Enums.ValidationStatus.VALID,
                USP = Domain.Enums.ValidationStatus.VALID,
            };

            var customerDTO = _mapper.Map<CustomerDTO>(customer);

            Assert.Equal(customer.ISR, customerDTO.ISR);
            Assert.Equal(customer.LastUpdated, customerDTO.LastUpdated);
            Assert.Equal(customer.Campaign, customerDTO.Campaign);
            Assert.Equal(customer.FederalTaxID, customerDTO.FederalTaxID);
            Assert.Equal(customer.CompanyName, customerDTO.CompanyName);
            Assert.Equal(customer.QuoteTime, customerDTO.QuoteTime);
            Assert.Equal(customer.GAMA, customerDTO.GAMA);
            Assert.Equal(customer.NewEasy, customerDTO.NewEasy);
            Assert.Equal(customer.SFDC, customerDTO.SFDC);
            Assert.Equal(customer.USP, customerDTO.USP);
        }

        [Fact]
        public void ConvertCustomerDTOToCustomer()
        {
            CustomerDTO customerDTO = new CustomerDTO()
            {
                ISR = "Viera, Aline",
                LastUpdated = DateTime.MinValue,
                Campaign = "OPEN QUOTES",
                CustomerNumber = "113576911",
                FederalTaxID = "CPF 80849415187",
                CompanyName = "WAGNER AUGUSTO DO AMARAL",
                QuoteTime = 0,
                GAMA = Domain.Enums.ValidationStatus.VALID,
                NewEasy = Domain.Enums.ValidationStatus.VALID,
                SFDC = Domain.Enums.ValidationStatus.VALID,
                USP = Domain.Enums.ValidationStatus.VALID,
            };

            var customer = _mapper.Map<Customer>(customerDTO);

            Assert.Equal(0, customer.ID);
            Assert.Equal(customerDTO.ISR, customer.ISR);
            Assert.Equal(customerDTO.LastUpdated, customer.LastUpdated);
            Assert.Equal(customerDTO.Campaign, customer.Campaign);
            Assert.Equal(customerDTO.FederalTaxID, customer.FederalTaxID);
            Assert.Equal(customerDTO.CompanyName, customer.CompanyName);
            Assert.Equal(customerDTO.QuoteTime, customer.QuoteTime);
            Assert.Equal(customerDTO.GAMA, customer.GAMA);
            Assert.Equal(customerDTO.NewEasy, customer.NewEasy);
            Assert.Equal(customerDTO.SFDC, customer.SFDC);
            Assert.Equal(customerDTO.USP, customer.USP);
        }

        [Fact]
        public void ConvertCampaignToCampaignDTO()
        {
            Campaign campaign = new Campaign()
            {
                ID = 0,
                Name = "open quotes",
                Description = "description",
                StartDate = DateTime.MinValue,
                EndDate = DateTime.MaxValue
            };

            var campaignDTO = _mapper.Map<CampaignDTO>(campaign);

            Assert.Equal(campaignDTO.ID, campaign.ID);
            Assert.Equal(campaignDTO.Name, campaign.Name);
            Assert.Equal(campaignDTO.Description, campaign.Description);
            Assert.Equal(campaignDTO.StartDate, campaign.StartDate);
            Assert.Equal(campaignDTO.EndDate, campaign.EndDate);
        }

        [Fact]
        public void ConvertCampaignDTOToCampaign()
        {
            CampaignDTO campaignDTO = new CampaignDTO()
            {
                ID = 0,
                Name = "open quotes",
                Description = "description",
                StartDate = DateTime.MinValue,
                EndDate = DateTime.MaxValue
            };

            var campaign = _mapper.Map<Campaign>(campaignDTO);

            Assert.Equal(campaignDTO.ID, campaign.ID);
            Assert.Equal(campaignDTO.Name, campaign.Name);
            Assert.Equal(campaignDTO.Description, campaign.Description);
            Assert.Equal(campaignDTO.StartDate, campaign.StartDate);
            Assert.Equal(campaignDTO.EndDate, campaign.EndDate);
        }
    }
}
