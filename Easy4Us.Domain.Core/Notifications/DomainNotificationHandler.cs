﻿using Easy4Us.Domain.Core.Interfaces;
using Easy4Us.Infra.CrossCutting.Log.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Easy4Us.Domain.Core.Notifications
{
    public class DomainNotificationHandler : IHandler<DomainNotification>
    {
        private List<DomainNotification> _notifications;
        private ILogger _logger;

        public DomainNotificationHandler(ILogger logger)
        {
            ClearNotifications();
            _logger = logger;
        }

        public void Handle(DomainNotification message)
        {
            _logger.LogWarning(message.Value);
            if (!_notifications.Any(x => x.Value.Trim().ToUpper().Equals(message.Value.Trim().ToUpper())))
            {
                _notifications.Add(message);
            }
        }

        public virtual List<DomainNotification> GetNotifications()
        {
            return _notifications;
        }

        public virtual IEnumerable<DomainNotification> Notify()
        {
            return GetNotifications();
        }

        public virtual bool HasNotifications()
        {
            return GetNotifications().Any();
        }

        public void Dispose()
        {
            _notifications = null;
            ClearNotifications();
        }

        public void ClearNotifications()
        {
            _notifications = new List<DomainNotification>();
        }

        public void LogInfo(string message)
        {
            Console.WriteLine(message);
            _logger.LogInfo(message);
        }

        public void LogWarning(string message)
        {
            Console.WriteLine(message);
            _logger.LogWarning(message);
        }

        public void LogError(Exception message, string additionalInfo = null)
        {
            Console.WriteLine(message);
            _logger.LogError(message, additionalInfo);
        }
    }
}
