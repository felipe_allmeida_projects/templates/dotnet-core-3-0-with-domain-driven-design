﻿using Easy4Us.Domain.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy4Us.Domain.Core.Notifications
{
    public class DomainNotification : IDomainEvent
    {
        public string Key { get; }
        public string Value { get; }
        public DateTime DateOcurrance { get; }
        public int Version { get; }

        public DomainNotification(string key, string value)
        {
            Version = 1;
            Key = key;
            Value = value;
            DateOcurrance = DateTime.Now;
        }
    }
}
