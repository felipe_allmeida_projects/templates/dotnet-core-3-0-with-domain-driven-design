﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy4Us.Domain.Core.Interfaces
{
    public interface IHandler<T> : IDisposable where T : IDomainEvent
    {
        void Handle(T args);
        IEnumerable<T> Notify();
        bool HasNotifications();
        List<T> GetNotifications();
        void ClearNotifications();
        void LogInfo(string message);
        void LogWarning(string message);
        void LogError(Exception ex, string additionalInfo = null);
    }
}
