﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy4Us.Domain.Core.Interfaces
{
    public interface IDomainEvent
    {
        int Version { get; }
        DateTime DateOcurrance { get; }
    }
}
