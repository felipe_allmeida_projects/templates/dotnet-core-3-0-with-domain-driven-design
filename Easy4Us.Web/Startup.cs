using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Easy4Us.Web.Data;
using Easy4Us.Infra.CrossCutting.IoC;
using Easy4Us.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using Easy4Us.Web.Configuration;
using Easy4Us.Infra.CrossCutting.SharePoint.Providers;
using Easy4Us.Infra.CrossCutting.Common.Providers;

namespace Easy4Us.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;            
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();

            services.AddDbContext<Easy4UsContext>(
                option => option.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.Configure<SharePointSettingsProvider>(Configuration.GetSection("Services").GetSection("SharePoint"));
            services.Configure<UserAccessorSettingsProvider>(Configuration.GetSection("Helpers").GetSection("UserAccessor"));

            // .NET Native DI Abstraction
            RegisterServices(services);

            services.AddAutoMapperSetup();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }

        private static void RegisterServices(IServiceCollection services)
        {
            // Adding dependencies from another layers (isolated from Presentation)
            NativeInjectorBootStrapper.RegisterServices(services);
        }
    }
}
