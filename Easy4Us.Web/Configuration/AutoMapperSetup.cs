﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Easy4Us.Application.AutoMapper;
using System;

namespace Easy4Us.Web.Configuration
{
    public static class AutoMapperSetup
    {
        public static void AddAutoMapperSetup(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            // DomainToDTOMappingProfile is pointing to the Easy4Us.Application Assembly
            services.AddAutoMapper(typeof(DomainToDTOMappingProfile));

            AutoMapperConfig.RegisterMappings();
        }
    }
}
