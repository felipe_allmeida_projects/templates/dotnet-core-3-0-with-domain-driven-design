﻿using Easy4Us.Application.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Easy4Us.Web.Data
{
    public class CampaignDetailsData
    {
        public CampaignDTO Campaign { get; private set; }
        public IEnumerable<CampaignDTO> Campaigns { get; private set; }
        public int CustomerNumber { get; private set; }
        public int Cleared { get; private set; }
        public string Updated { get; private set; }

        public void SetCampaignDetails(CampaignDTO selected, IEnumerable<CampaignDTO> campaigns, IEnumerable<CustomerDTO> customers)
        {
            CustomerNumber = customers.Count();
            Campaigns = campaigns;
            Cleared = customers.Where(x => x.USP == Domain.Enums.ValidationStatus.VALID &&
                                         x.GAMA == Domain.Enums.ValidationStatus.VALID &&
                                         x.SFDC == Domain.Enums.ValidationStatus.VALID &&
                                         x.NewEasy == Domain.Enums.ValidationStatus.VALID).Count();
            Updated = customers.LastOrDefault().LastUpdated.ToString("dd MMM, yyyy HH:mm tt");
        }
    }
}
