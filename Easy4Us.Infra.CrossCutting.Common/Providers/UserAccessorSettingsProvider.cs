﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy4Us.Infra.CrossCutting.Common.Providers
{
    public class UserAccessorSettingsProvider
    {
        public bool OverrideUser { get; set; }
        public List<string> TestUsers { get; set; }
    }
}
