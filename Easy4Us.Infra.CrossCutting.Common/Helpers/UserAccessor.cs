﻿using Easy4Us.Infra.CrossCutting.Common.Interfaces;
using Easy4Us.Infra.CrossCutting.Common.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Text;

namespace Easy4Us.Infra.CrossCutting.Common.Helpers
{
    public class UserAccessor : IUserAccessor
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserAccessorSettingsProvider _settings;

        private string _userName;
        private string _userEmail;
        private string _userDisplayName;

        public UserAccessor(IOptions<UserAccessorSettingsProvider> iOptionsConfig, IHttpContextAccessor httpContextAccessor)
        {
            _settings = iOptionsConfig.Value;
            _httpContextAccessor = httpContextAccessor;

            if (_settings.OverrideUser)
            {
                var rndIndex = new Random().Next(0, _settings.TestUsers.Count - 1);
                _userDisplayName = _settings.TestUsers[rndIndex];
                return;
            }

            LoadCurrentUserData();
        }

        public string GetCurrentUserDisplayName()
        {
            return _userDisplayName;
        }

        public string GetCurrentUserEmail()
        {
            return _userEmail;
        }

        public string GetCurrentUserName()
        {
            return _userName;
        }

        private void LoadCurrentUserData()
        {
            _userName = _httpContextAccessor.HttpContext.User.Identity.Name;

            var directorySearcher = new DirectorySearcher();
            directorySearcher.Filter = $"(&(objectClass=user)(objectcategory=person)(name={_userName.Split('\\')[1]}))";

            var searchResult = directorySearcher.FindOne();

            _userEmail = searchResult.Properties["mail"][0].ToString();
            _userDisplayName = searchResult.Properties["displayname"][0].ToString();
        }
    }
}
