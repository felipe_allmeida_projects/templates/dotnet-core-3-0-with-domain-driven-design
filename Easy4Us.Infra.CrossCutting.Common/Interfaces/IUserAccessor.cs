﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy4Us.Infra.CrossCutting.Common.Interfaces
{
    public interface IUserAccessor
    {
        string GetCurrentUserName();
        string GetCurrentUserDisplayName();
        string GetCurrentUserEmail();
    }
}
