﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Easy4Us.Infra.CrossCutting.Common.Http
{
    public class HttpClientHelper
    { 
        public static async Task<TResponse> Get<TResponse>(string url, params KeyValuePair<string, string>[] parameters)
        {
            using (var client = new HttpClient())
            {
                var builder = new UriBuilder(url);
                var query = HttpUtility.ParseQueryString(builder.Query);

                parameters.ToList().ForEach(x => query[x.Key] = x.Value);

                builder.Query = query.ToString();

                var response = await client.GetAsync(builder.ToString());
                
                if (response.IsSuccessStatusCode)
                    return await response.Content.ReadAsAsync<TResponse>();

                return default;
            }
        }

        public static async Task<TResponse> Post<TData, TResponse>(string url, TData data)
        {
            using (var client = new HttpClient())
            {
                string myContent = JsonConvert.SerializeObject(data);
                byte[] buffer = Encoding.UTF8.GetBytes(myContent);
                ByteArrayContent byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

                var response = await client.PostAsync(url, byteContent);

                if (response.IsSuccessStatusCode)
                    return await response.Content.ReadAsAsync<TResponse>();

                return default;
            }

        }
    }
}
