﻿namespace Easy4Us.Infra.CrossCutting.SharePoint.Models
{
    public class NewEasyCustomer
    {
        public string CustomerNumber { get; set; }
        public string FederalTaxID { get; set; }
        public string CompanyName { get; set; }
        public string DealEvolution { get; set; }
        public string LOR { get; set; }
        public string CreatedBy { get; set; }
        public string ObservationLor { get; set; }
        public string Observation { get; set; }
        public string Created { get; set; }
        public string Modified { get; set; }
    }
}
