﻿namespace Easy4Us.Infra.CrossCutting.SharePoint.Models
{
    public class StrongerCustomer
    {
        public string ISR { get; set; }
        public string CustomerNumber { get; set; }
        public string FederalTaxID { get; set; }
        public string CompanyName { get; set; }
        public string Campaign { get; set; }
    }
}
