﻿using Easy4Us.Infra.CrossCutting.Common.Http;
using Easy4Us.Infra.CrossCutting.SharePoint.Interfaces;
using Easy4Us.Infra.CrossCutting.SharePoint.Models;
using Easy4Us.Infra.CrossCutting.SharePoint.Providers;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SP = Microsoft.SharePoint.Client;

namespace Easy4Us.Infra.CrossCutting.SharePoint.Services
{
    public class SharePointService : ISharePointService
    {
        private readonly SharePointSettingsProvider _settings;

        public SharePointService(IOptions<SharePointSettingsProvider> iOptionsConfig)
        {
            _settings = iOptionsConfig.Value;
        }

        public async Task<IEnumerable<StrongerCustomer>> GetStrongerCustomers()
        {
            string url = $"{_settings.BaseURL}/{_settings.StrongerEndpoint}";

            return await HttpClientHelper.Get<IEnumerable<StrongerCustomer>>(url);
        }

        public async Task<IEnumerable<StrongerCustomer>> GetStrongerCustomers(string isr)
        {
            string url = $"{_settings.BaseURL}/{_settings.StrongerEndpoint}";

            return await HttpClientHelper.Get<IEnumerable<StrongerCustomer>>(url, new KeyValuePair<string, string>(nameof(isr), isr));
        }

        public async Task<IEnumerable<NewEasyCustomer>> GetNewEasyCustomers()
        {
            string url = $"{_settings.BaseURL}/{_settings.NewEasyEndpoint}";

            return await HttpClientHelper.Get<IEnumerable<NewEasyCustomer>>(url);
        }
    }
}
