﻿using Easy4Us.Infra.CrossCutting.SharePoint.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Easy4Us.Infra.CrossCutting.SharePoint.Interfaces
{
    public interface ISharePointService
    {
        Task<IEnumerable<StrongerCustomer>> GetStrongerCustomers();
        Task<IEnumerable<StrongerCustomer>> GetStrongerCustomers(string isr);
        Task<IEnumerable<NewEasyCustomer>> GetNewEasyCustomers();
    }
}
