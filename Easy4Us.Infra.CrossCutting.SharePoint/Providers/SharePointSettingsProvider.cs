﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy4Us.Infra.CrossCutting.SharePoint.Providers
{
    public class SharePointSettingsProvider
    {
        public string BaseURL { get; set; }
        public string StrongerEndpoint { get; set; }
        public object NewEasyEndpoint { get; set; }
    }
}
